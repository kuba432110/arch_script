#!/bin/sh
function error
{
    [[ ${3} ]]&&
    echo -e "\x1b[31mError!\x1b[33m ${1}...\x1b[0m"||
    {
        echo -e "\x1b[31mError!\x1b[33m ${1}, exiting...\x1b[0m"
        exit ${2}
    }
}
function precheck
{
    echo -e "\x1b[34mChecking compatibility...\x1b[0m"
    [[ $UID = 0 ]]&&
    echo "root user detected..."||
    error "Only root can run this script" 1
    [[ -e /sys/firmware/efi/efivars ]]&&
    echo "EFI system detected..."||
    error "Non EFI system" 1
    ( ping -c 1 archlinux.org &> /dev/null )&&
    echo "Internet connection detected..."||
    error "No internet connection" 1
    echo -e "\x1b[32mDone!\x1b[0m"
}
function fsbasic
{
    PS3="Select a disk to partition: "
    echo -e "\x1b[34mPreparing filesystem...\x1b[0m"
    #creating partitions if not present
    {
        [[ -e /dev/disk/by-partlabel/ROOT ]]&&
        [[ -e /dev/disk/by-partlabel/SWAP ]]&&
        [[ -e /dev/disk/by-partlabel/EFI  ]]
    }&&
    echo "Partitions detected..."||
    {
        echo -e "\x1b[34mPartitions named: ROOT, SWAP, EFI must exist in order to continue installation. Preparing separate HOME partition is recommended as well.\x1b[0m"
        until
        {
            [[ -e /dev/disk/by-partlabel/ROOT ]]&&
            [[ -e /dev/disk/by-partlabel/SWAP ]]&&
            [[ -e /dev/disk/by-partlabel/EFI  ]]
        }
        do
            select disk in $( lsblk -ndo PATH )
            do
                [[ $disk = '' ]]&&continue
                cgdisk $disk||
                error "Cannot open cgdisk" 2
                break
            done
            {
                [[ -e /dev/disk/by-partlabel/ROOT ]]&&
                [[ -e /dev/disk/by-partlabel/SWAP ]]&&
                [[ -e /dev/disk/by-partlabel/EFI  ]]
            }||
            error "Essential partitions not detected" 2 1
        done
    }&&
    echo "Partitions created..."
    #formatting partitions
    #ROOT and SWAP
    while true
    do
        echo -e "\x1b[33mDo you agree to wipe your ROOT and SWAP?\x1b[0m\nProceed? [y/N]"
        read choice
        {
            [[ $choice = y* ]]||
            [[ $choice = Y* ]]
        }&&
        {
            #check if HOME is inside ROOT
            [[ -e /dev/disk/by-partlabel/HOME ]]&&
            break||
            {
                echo -e "\x1b[31mAre you RALLY sure you want to WIPE ROOT? (Your data might be still in there... :3)\x1b[0m\nProceed? [doit/NO]"
                read choice
                [[  $choice = doit ]]&&
                break||
                continue
            }
        }||
        error "Partitions need bo be formatted before installation" 2
    done
    mkfs.ext4 /dev/disk/by-partlabel/ROOT &> /dev/null||
    error "Formating ROOT failed" 2
    mkswap /dev/disk/by-partlabel/SWAP &> /dev/null||
    error "Formating SWAP failed" 2
    echo "ROOT and SWAP wiped..."
    #EFI
    echo -e "\x1b[33mDo you want to wipe EFI?\x1b[0m\nProceed? [y/N]"
    read choice
    {
        [[ $choice = y* ]]||
        [[ $choice = Y* ]]
    }&&
    {
        mkfs.fat -F32 /dev/disk/by-partlabel/EFI &> /dev/null||
        error "Formating EFI failed" 2
    }
    #HOME if exists separately
    [[ -e /dev/disk/by-partlabel/HOME ]]&&
    {
        while true
        do
            echo -e "\x1b[33mDo you want to wipe HOME?.\x1b[0m\nProceed? [y/N]"
            read choice
            {
                [[ $choice = y* ]]||
                [[ $choice = Y* ]]
            }&&
            {
                echo -e "\x1b[31mAre you RALLY sure you want to WIPE HOME? (Your data might be still in there... :3)\x1b[0m\nProceed? [doit/NO]"
                read choice
                [[ $choice = doit ]]&&
                {
                    mkfs.ext4 /dev/disk/by-partlabel/HOME &> /dev/null||
                    error "Formating HOME failed" 2
                    echo -e "\x1b[31mYO DATA GONE DAWG\x1b[0m"
                    break
                }||continue
             }||
             break
         done
    }
    #mounting partitions
    mount /dev/disk/by-partlabel/ROOT /mnt &> /dev/null||
    error "Mounting ROOT failed" 2
    swapon /dev/disk/by-partlabel/SWAP &> /dev/null||
    error "Mounting SWAP failed" 2
    mount --mkdir /dev/disk/by-partlabel/EFI /mnt/boot &> /dev/null||
    error "Mounting EFI failed" 2
    [[ -e /dev/disk/by-partlabel/HOME ]]&&
    {
        mount --mkdir /dev/disk/by-partlabel/HOME /mnt/home &> /dev/null||
        error "Mounting HOME failed" 2
    }
    echo -e "\x1b[32mDone!\x1b[0m"
}
function install
{
    pacstrap /mnt base linux-zen
}
precheck
fsbasic
